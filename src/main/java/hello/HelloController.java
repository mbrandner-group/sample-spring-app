package hello;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import no.finn.unleash.DefaultUnleash;
import no.finn.unleash.Unleash;
import no.finn.unleash.util.UnleashConfig;



@RestController
public class HelloController {

    UnleashConfig config = UnleashConfig.builder()
            .appName("production")
            .instanceId("2sCnvMadJ28LenHHxndG")
            .unleashAPI("https://gitlab.com/api/v4/feature_flags/unleash/29820688")
            .build();

    Unleash unleash = new DefaultUnleash(config);
    
    @RequestMapping("/")
    public String index() {
        String style = "<style type='text/css' media='screen'>";
        style += "body { background-color: silver; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); color: orange; font-size: 250%; }";
        style += "</style>";
        
        String message;
        
        
        if(unleash.isEnabled("greeting")) {
            message = "Feature flag Enabled! Hello from GitLab";
        } else {
            message = "Hello from GitLab without feature flag";
        }
        
        String body = "<body>" + message + "</body>";

        return style + body;
    }

}
